# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|

## Website Detail Description
遊戲玩法:  
進入menu選擇遊戲關卡1 2 3(keyboard 1, 2, 3)，按下ENTER開始    
上下左右箭頭移動player，空白建發射子彈，遊戲畫面右上角顯示剩餘儲備戰機(player生命值)  
keyboard P 為 pause 鍵，再按一次即繼續遊戲   
GameOver的時候按下 R(restart)為該關卡重新開始，若Q(quit)則是回到menu重新選擇欲挑戰之關卡。  
# Basic Components Description :   
1. Jucify mechanisms :  
    三個關卡，1:easy 2: medium 3: difficult  
    分數達到1000以後子彈為兩道射擊，敵人有三種隨機出現，灰色戰機只有一條生命，藍色戰機兩條生命(必須擊中兩次才會死)，紅色戰機有三條生命(必須擊中三次才會死)，直到最後會遇到boss，必需擊中30次才能打掉，會得到1000~1500分的獎勵。  
2. Animations :   
    player有動畫，機身往左飛與往右飛時有不同的圖案。  
3. Particle Systems :   
    子彈打到機身時，若尚未爆炸會有向後彈一下的效果，若生命值用完則會有爆炸效果。    
4. Sound effects :  
    有background music和 子彈效果與爆炸效果的音效，按下 Z 可以增加音量， 按下 X 可以減小音量，總共有六個音量可以調整(0~5)。    
5. Leaderboard : [xxxx]  

# Bonus Functions Description : 
1. Boss:    
    每一個關卡最後都有一個boss，可以發射多個子彈，必須打掉boss才可以完成任務成為贏家。  
    boss的射擊速度隨著關卡變難提升，其中stage 1有5發子彈同時發射，stage 2 3 是有7發子彈同時發射 。  
    
2. 地圖:  
    從Tiled做出500 * 1500的地圖。地圖會隨著遊戲的進行轉動，玩家也會跟著移動。    
    用camera的移動做出地圖捲動效果，其中text等物件都是fixed To camera = true的狀態。  
3. Highscore:  
    秀出本次遊戲最佳紀錄  
    用game.global.highestscore來記錄，每一次遊戲結束即刷新  
4. 難易度設計
    每一關卡的小兵以及boss都有不同的射擊速度和移動速度，增加玩家過關的困難度，並且縮短產生敵人的週期，加快敵人生成時間，增加難度 
