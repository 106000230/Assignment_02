var game = new Phaser.Game(500, 450, Phaser.AUTO, "canvas");

//global variable
game.global = {
    vol: 0.2,
    score: 0,
    stage: 1,
    highestscore: 0
};
//add all states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('over', overState);
//game.state.add('gameRestart', gameRestartState);
//start boot in begining
game.state.start('boot');
