var overState = {
    create: function(){
        if(game.global.score > game.global.highestscore){
            game.global.highestscore = game.global.score;
        } 
        game.stage.background = '#3498db';
        //display nameof game
        var overLabel = game.add.text(game.width/2, 80, 'Game Over', {font:'50px Papyrus', fill:'#ffffff'});
        overLabel.anchor.setTo(0.5, 0.5);
        //show the score 
        var scoreLabel = game.add.text(game.width/2, 150, 'Score: ' + game.global.score, {font:'25px Papyrus', fill:'#ffffff'});
        scoreLabel.anchor.setTo(0.5, 0.5);
        var highscoreLabel = game.add.text(game.width/2, 200, 'Highscore: ' + game.global.highestscore, {font:'25px Papyrus', fill:'#ffffff'});
        highscoreLabel.anchor.setTo(0.5, 0.5);
        //press enter start game
        var restartLabel = game.add.text(game.width/2, 270, '(R)restart or (Q)quit', {font:'25px Papyrus', fill:'#ffffff'});
        restartLabel.anchor.setTo(0.5, 0.5);

        //key detect
        var KeyR = game.input.keyboard.addKey(Phaser.Keyboard.R);
        KeyR.onDown.add(this.restart, this);

        var KeyQ = game.input.keyboard.addKey(Phaser.Keyboard.Q);
        KeyQ.onDown.add(this.quit, this);
    },
    restart: function(){
        game.global.score = 0;
        //game.global.stage = 1;
        game.state.start('play');
    },
    quit: function(){
        game.global.stage = 1;
        game.state.start('menu');
    }
}