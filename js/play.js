Enemy1 =  function(index, game, player, bullets){
    this.game = game;
    this.player = player;
    this.bullets = bullets;
    this.fireRate = 1000;
    this.nextFire = 0;
    this.die = false;
    
    this.alive = true;
    if(index < 20){
        this.idx = game.rnd.between(0, 2);
        if(this.idx == 0) {
            this.enemy1 = game.add.sprite(30 * game.rnd.between(2, 15), game.camera.y - 100, 'enemy1');
            this.enemy1.scale.setTo(0.04, 0.04);
            this.health = 1;
        }
        else if(this.idx == 1){
            this.enemy1 = game.add.sprite(30 * game.rnd.between(2, 15), game.camera.y - 100, 'enemy2');
            this.enemy1.scale.setTo(0.04, 0.04);
            this.health = 2;
        } 
        else {
            this.enemy1 = game.add.sprite(30 * game.rnd.between(2, 15), game.camera.y - 100, 'enemy3');
            this.enemy1.scale.setTo(0.1, 0.1);
            this.health = 3;
        }
    }
    else if(index == 20) {
        this.idx = 3;
        this.health = 30;
        this.enemy1 = game.add.sprite(game.camera.width/2, 50, 'boss');
        this.enemy1.scale.setTo(0.1, 0.1);
    }
    this.enemy1.anchor.setTo(0.5, 0.5);

    this.enemy1.name = index.toString();

    game.physics.enable(this.enemy1, Phaser.Physics.ARCADE);
    game.physics.collideWorldBounds = true;
    this.enemy1.body.immovable = false;
    this.enemy1.outOfBoundsKill = true;

    if(index < 20){
        if(this.idx == 0){
            if(game.global.stage == 1){
                this.enemy1.body.velocity.y = 20;
            } 
            else if(game.global.stage == 2){
                this.enemy1.body.velocity.y = 25;
            }
            else{
                this.enemy1.body.velocity.y = 30;
            }
        } 
        else if(this.idx == 1){
            if(game.global.stage == 1){
                this.enemy1.body.velocity.x = game.rnd.between(-20, 20);
                this.enemy1.body.velocity.y = 25;
            } 
            else if(game.global.stage == 2){
                this.enemy1.body.velocity.x = game.rnd.between(-30, 30);
                this.enemy1.body.velocity.y = 30;
            }
            else{
                this.enemy1.body.velocity.x = game.rnd.between(-30, 30);
                this.enemy1.body.velocity.y = 40;
            }
        }
        else if(this.idx == 2) {
            if(game.global.stage == 1){
                this.enemy1.body.velocity.x = game.rnd.between(-40, 40);
                this.enemy1.body.velocity.y = game.rnd.between(20, 30);
            } 
            else if(game.global.stage == 2){
                this.enemy1.body.velocity.x = game.rnd.between(-40, 40);
                this.enemy1.body.velocity.y = game.rnd.between(30, 40);
            }
            else{
                this.enemy1.body.velocity.x = game.rnd.between(-40, 40);
                this.enemy1.body.velocity.y = game.rnd.between(40, 50);
            }
        }
    }
    else if(index == 20){
        //game.add.tween(this.enemy1).to({y: 50}, 0).start();
        this.enemy1.body.velocity.y = 0;
        this.enemy1.body.velocity.x = 0;
        //this.enemy1.body.collideWorldBounds = true;
        //this.enemy1.body.bounce.setTo(1, 1);
    }
}
Enemy1.prototype.damage =  function(){
    //in bound of camera then kill, or ignore the bullet
    if(this.enemy1.y >= game.camera.y && this.enemy1.y <= game.camera.y + 450) this.health -= 1;
    if(this.health <= 0){
        this.alive = false;
        this.enemy1.kill();
        /*
        else{
            this.die = true;
            this.enemy1.loadTexture('hp');
            //this.enemy1.scale.setTo(0,1, 0.1);
            this.enemy1.body.velocity.x = game.rnd.between(-10, 10);
            this.enemy1.body.velocity.y = 10;
        }
        */
        return true;
    }
    return false;
}
Enemy1.prototype.update = function(){
    if(this.game.physics.arcade.distanceBetween(this.enemy1, this.player) < 450 && ( this.enemy1.y > game.camera.y && this.enemy1.y < game.camera.y + 450)){
        if(this.game.time.now > this.nextFire && this.bullets.countDead() > 0){
            // shoot rate
            if(game.global.stage == 1){
                if(this.idx != 3)this.nextFire = this.game.time.now + 2000;
                else this.nextFire = this.game.time.now + 1700;
            }
            else if(game.global.stage == 2){
                if(this.idx != 3)this.nextFire = this.game.time.now + 1800;
                else this.nextFire = this.game.time.now + 1500;
            }
            else if(game.global.stage == 3){
                if(this.idx != 3)this.nextFire = this.game.time.now + 1600;
                else this.nextFire = this.game.time.now + 1200;
            }
           
            
            //stage1 bullet
            if(game.global.stage == 1){
                if(this.idx == 0){
                    var bullet = this.bullets.getFirstDead(); 
                    bullet.reset(this.enemy1.x, this.enemy1.y);
                    bullet.body.velocity.y = 50;
                }
                else if(this.idx == 1) {
                    var bullet1 = this.bullets.getFirstDead(); 
                    bullet1.reset(this.enemy1.x-12, this.enemy1.y);
                    var bullet2 = this.bullets.getFirstDead(); 
                    bullet2.reset(this.enemy1.x+12, this.enemy1.y);
                    bullet1.body.velocity.y = 50;
                    bullet2.body.velocity.y = 50;
                }
                else if(this.idx == 2){
                    var bullet1 = this.bullets.getFirstDead(); 
                    bullet1.reset(this.enemy1.x-16, this.enemy1.y);
                    var bullet2 = this.bullets.getFirstDead(); 
                    bullet2.reset(this.enemy1.x, this.enemy1.y);
                    var bullet3 = this.bullets.getFirstDead(); 
                    bullet3.reset(this.enemy1.x+16, this.enemy1.y);
                    bullet1.body.velocity.x = -40;
                    bullet1.body.velocity.y = 50;
                    bullet2.body.velocity.x = 0;
                    bullet2.body.velocity.y = 50;
                    bullet3.body.velocity.x = 40;
                    bullet3.body.velocity.y = 50;
                }
                else{
                    var bullet1 = this.bullets.getFirstDead(); 
                    bullet1.reset(this.enemy1.x-12, this.enemy1.y);
                    var bullet2 = this.bullets.getFirstDead(); 
                    bullet2.reset(this.enemy1.x-6, this.enemy1.y);
                    var bullet3 = this.bullets.getFirstDead(); 
                    bullet3.reset(this.enemy1.x, this.enemy1.y);
                    var bullet4 = this.bullets.getFirstDead(); 
                    bullet4.reset(this.enemy1.x+6, this.enemy1.y);
                    var bullet5 = this.bullets.getFirstDead(); 
                    bullet5.reset(this.enemy1.x+12, this.enemy1.y);
                    bullet1.body.velocity.x = -120;
                    bullet1.body.velocity.y = 100;
                    bullet2.body.velocity.x = -60;
                    bullet2.body.velocity.y = 100;
                    bullet3.body.velocity.x = 0;
                    bullet3.body.velocity.y = 100;
                    bullet4.body.velocity.x = 60;
                    bullet4.body.velocity.y = 100;
                    bullet5.body.velocity.x = 120;
                    bullet5.body.velocity.y = 100;

                }
                
            } 
            //stage2 bullet           
            else if(game.global.stage == 2){
                if(this.idx == 0){
                    var bullet = this.bullets.getFirstDead(); 
                    bullet.reset(this.enemy1.x, this.enemy1.y);
                    bullet.body.velocity.y = 150;
                }
                else if(this.idx == 1) {
                    var bullet1 = this.bullets.getFirstDead(); 
                    bullet1.reset(this.enemy1.x-12, this.enemy1.y);
                    var bullet2 = this.bullets.getFirstDead(); 
                    bullet2.reset(this.enemy1.x+12, this.enemy1.y);
                    bullet1.body.velocity.y = 150;
                    bullet2.body.velocity.y = 150;
                }
                else if(this.idx == 2){
                    var bullet1 = this.bullets.getFirstDead(); 
                    bullet1.reset(this.enemy1.x-16, this.enemy1.y);
                    var bullet2 = this.bullets.getFirstDead(); 
                    bullet2.reset(this.enemy1.x, this.enemy1.y);
                    var bullet3 = this.bullets.getFirstDead(); 
                    bullet3.reset(this.enemy1.x+16, this.enemy1.y);
                    bullet1.body.velocity.x = -40;
                    bullet1.body.velocity.y = 150;
                    bullet2.body.velocity.x = 0;
                    bullet2.body.velocity.y = 150;
                    bullet3.body.velocity.x = 40;
                    bullet3.body.velocity.y = 150;
                }
                else{
                    var bullet1 = this.bullets.getFirstDead(); 
                    bullet1.reset(this.enemy1.x-12, this.enemy1.y);
                    var bullet2 = this.bullets.getFirstDead(); 
                    bullet2.reset(this.enemy1.x-8, this.enemy1.y);
                    var bullet3 = this.bullets.getFirstDead(); 
                    bullet3.reset(this.enemy1.x-4, this.enemy1.y);
                    var bullet4 = this.bullets.getFirstDead(); 
                    bullet4.reset(this.enemy1.x, this.enemy1.y);
                    var bullet5 = this.bullets.getFirstDead(); 
                    bullet5.reset(this.enemy1.x+4, this.enemy1.y);
                    var bullet6 = this.bullets.getFirstDead(); 
                    bullet6.reset(this.enemy1.x+8, this.enemy1.y);
                    var bullet7 = this.bullets.getFirstDead(); 
                    bullet7.reset(this.enemy1.x+12, this.enemy1.y);
                    bullet1.body.velocity.x = -120;
                    bullet1.body.velocity.y = 170;
                    bullet2.body.velocity.x = -80;
                    bullet2.body.velocity.y = 170;
                    bullet3.body.velocity.x = -40;
                    bullet3.body.velocity.y = 170;
                    bullet4.body.velocity.x = 0;
                    bullet4.body.velocity.y = 170;
                    bullet5.body.velocity.x = 40;
                    bullet5.body.velocity.y = 170;
                    bullet6.body.velocity.x = 80;
                    bullet6.body.velocity.y = 170;
                    bullet7.body.velocity.x = 120;
                    bullet7.body.velocity.y = 170;
                }
                
            }
            //stage3 bullet
            else {
                if(this.idx == 0){
                    var bullet = this.bullets.getFirstDead(); 
                    bullet.reset(this.enemy1.x, this.enemy1.y);
                    bullet.body.velocity.y = 150;
                }
                else if(this.idx == 1) {
                    var bullet1 = this.bullets.getFirstDead(); 
                    bullet1.reset(this.enemy1.x-12, this.enemy1.y);
                    var bullet2 = this.bullets.getFirstDead(); 
                    bullet2.reset(this.enemy1.x+12, this.enemy1.y);
                    bullet1.body.velocity.y = 170;
                    bullet2.body.velocity.y = 170;
                }
                else if(this.idx == 2){
                    var bullet1 = this.bullets.getFirstDead(); 
                    bullet1.reset(this.enemy1.x-16, this.enemy1.y);
                    var bullet2 = this.bullets.getFirstDead(); 
                    bullet2.reset(this.enemy1.x, this.enemy1.y);
                    var bullet3 = this.bullets.getFirstDead(); 
                    bullet3.reset(this.enemy1.x+16, this.enemy1.y);
                    bullet1.body.velocity.x = -40;
                    bullet1.body.velocity.y = 170;
                    bullet2.body.velocity.x = 0;
                    bullet2.body.velocity.y = 170;
                    bullet3.body.velocity.x = 40;
                    bullet3.body.velocity.y = 170;
                }
                else{
                    var bullet1 = this.bullets.getFirstDead(); 
                    bullet1.reset(this.enemy1.x-12, this.enemy1.y);
                    var bullet2 = this.bullets.getFirstDead(); 
                    bullet2.reset(this.enemy1.x -8, this.enemy1.y);
                    var bullet3 = this.bullets.getFirstDead(); 
                    bullet3.reset(this.enemy1.x-4, this.enemy1.y);
                    var bullet4 = this.bullets.getFirstDead(); 
                    bullet4.reset(this.enemy1.x, this.enemy1.y);
                    var bullet5 = this.bullets.getFirstDead(); 
                    bullet5.reset(this.enemy1.x+4, this.enemy1.y);
                    var bullet6 = this.bullets.getFirstDead(); 
                    bullet6.reset(this.enemy1.x+8, this.enemy1.y);
                    var bullet7 = this.bullets.getFirstDead(); 
                    bullet7.reset(this.enemy1.x+12, this.enemy1.y);
                    bullet1.body.velocity.x = -120;
                    bullet1.body.velocity.y = 200;
                    bullet2.body.velocity.x = -80;
                    bullet2.body.velocity.y = 200;
                    bullet3.body.velocity.x = -40;
                    bullet3.body.velocity.y = 200;
                    bullet4.body.velocity.x = 0;
                    bullet4.body.velocity.y = 200;
                    bullet5.body.velocity.x = 400;
                    bullet5.body.velocity.y = 200;
                    bullet6.body.velocity.x = 80;
                    bullet6.body.velocity.y = 200;
                    bullet7.body.velocity.x = 120;
                    bullet7.body.velocity.y = 200;

                }
               
            }
        }
    }
}

var playState = {
    preload: function() {},
    create: function() {
        this.gamestart = true;
        //create audio
        explosion = game.add.audio('explosion');
        shoot = game.add.audio('playershoot');
        game_bg = game.add.audio('game_bg');
        game_bg.loop = true;
        game_bg.play();
        game_bg.volume = game.global.vol;

        game.physics.startSystem(Phaser.Physics.ARCADE);
        
        this.map = game.add.tilemap('map');
        this.map.addTilesetImage('Desert', 'tileset');
        this.maplayer = this.map.createLayer('Layer1');
        this.maplayer.resizeWorld();
        
        
        /* player */
        this.player = game.add.sprite(game.width/2, 5000, 'player');
        //this.player.scale.setTo(0.2,0.2);
        this.player.anchor.setTo(0.5, 0.5); // Set the anchor point of player to center.
        this.plyerlife = 5;
        nowtype = 1;
        this.boss = false;
        
        /* add player bullets */
        bullets = game.add.group();
        bullets.enableBody = true;
        bullets.physicsBodyType = Phaser.Physics.ARCADE;
        bullets.createMultiple(1000, 'bullet');
        bullets.setAll('checkWorldBounds', true);
        bullets.setAll('outOfBoundsKill', true);
        nextFire = 0;

        /* add enemy1 */
        //enemy bullets
        enemy1bullets = game.add.group();
        
        enemy1bullets.enableBody = true;
        enemy1bullets.physicsBodyType = Phaser.Physics.ARCADE;
        enemy1bullets.createMultiple(1000, 'ebullet');
        //enemy1bullets.scale.setTo(0.7, 0.7);
        enemy1bullets.setAll('anchor.x', 0.5);
        enemy1bullets.setAll('anchor.y', 0.5);
        enemy1bullets.setAll('checkWorldBounds', true);
        enemy1bullets.setAll('outOfBoundsKill', true);
        nextenemy = 0;
        enemycount = 0;
        
        enemies1 = [];
        enemies1Total = 5;
        enemies1Alive = 1;
        enemiesindex = 0;
        
        
        /* explosion animation */
        explosions = game.add.group();
        for(var i = 0; i < 10; i++){
            var explosionAnimation = explosions.create(0, 0, 'kaboom', [0], false);
            explosionAnimation.anchor.setTo(0.5, 0.5);
            explosionAnimation.animations.add('kaboom');
        }

        /* register keys */
        this.cursor = game.input.keyboard.createCursorKeys();
        this.spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        game.input.keyboard.addKeyCapture([ Phaser.Keyboard.SPACEBAR ]);
        var pauseKey = game.input.keyboard.addKey(Phaser.Keyboard.P);
        pauseKey.onDown.add(this.gamepause, this);

        var KeyZ = game.input.keyboard.addKey(Phaser.Keyboard.Z);
        var KeyX = game.input.keyboard.addKey(Phaser.Keyboard.X);
        KeyZ.onDown.add(this.volumeup, this);
        KeyX.onDown.add(this.volumedown, this);
        /* labels */
        scoreLabel = game.add.text(10, 10, 'score: ' + game.global.score, {font:'15px Papyrus', fill:'#ffffff'});
        scoreLabel.fixedToCamera = true;
        volLabel = game.add.text(10, 30, 'volume: 1' , {font:'15px Papyrus', fill: '#ffffff'});
        volLabel.fixedToCamera = true;
        pauseLabel = game.add.text(game.camera.width/2, game.camera.height/2, '', {font:'25px Papyrus', fill:'#000000'});
        pauseLabel.anchor.setTo(0.5, 0.5);
        pauseLabel.fixedToCamera = true;
        stageLabel = game.add.text(10, 50, 'stage: ' + game.global.stage , {font:'15px Papyrus', fill: '#ffffff'});
        stageLabel.fixedToCamera = true;
        gamemsg = game.add.text(game.camera.width/2, game.camera.height/2, '', {font:'28px Papyrus', fill: '#ffffff'});
        gamemsg.anchor.setTo(0.5, 0.5);
        gamemsg.fixedToCamera = true;

        playerlives = game.add.group();
        playerlives.fixedToCamera = true;
        for (var i = 0; i < 3; i++){
            var playership = playerlives.create(game.camera.width + 70 + (50 * i), 60, 'player');
            playership.anchor.setTo(0.5, 0.5);
            playership.angle = 90;
            playership.alpha = 0.7;
        }
        playerlives.scale.set(0.7, 0.7);
        
        
        /* reset camera, set camera rectangle with this.player*/
        //game.camera.follow(this.player, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);
        game.camera.y = 5000;
        game.physics.arcade.enable(this.player);
        this.player.autoCull = true;
        rect = new Phaser.Rectangle(0, 0, 500, 450);
        game.physics.enable(rect, Phaser.Physics.ARCADE);
    },

    update: function() {
        this.player.body.collideWorldBounds = true;
        game_bg.volume = game.global.vol;
        this.updatestage();
        this.callEnemy();
        this.movePlayer();
        this.playershoot();

        /* enemy1 update */
        enemies1Alive = 0;
        for(var i = 0; i< enemies1.length; i++){
            if(enemies1[i].alive){
                enemies1Alive++;
                //game.physics.arcade.overlap(this.player, enemies1[i].enemy1, this.playereat, null, this);
                game.physics.arcade.overlap(bullets, enemies1[i].enemy1, this.bulletHit, null, this);
                enemies1[i].update();
            }
        }
        /* this.player colllision */
        game.physics.arcade.overlap(enemy1bullets, this.player, this.playerdie, null, this);
        game.physics.arcade.collide(this.player, rect);

        /* test updating */
        scoreLabel.setText("score: " + game.global.score);
        //volLabel.setText("volume: " + game.global.vol);
        
        /* moving camera */
        this.movecamera();
         
        if(game.global.score > 1000) nowtype = 2;
    },

    movePlayer: function() {
        if(this.cursor.up.isDown && this.player.y > game.camera.y){
            this.player.loadTexture('player', 0);
            this.player.y -= 5;
        }
        if(this.cursor.down.isDown && this.player.y < game.camera.y+450){
            this.player.loadTexture('player', 0);
            this.player.y += 5;
        }
        if(this.cursor.left.isDown && this.player.x > game.camera.x){
            this.player.loadTexture('player_left', 0);
            this.player.x -= 5;
        } 
        if(this.cursor.right.isDown && this.player.x < game.camera.x + 500){
            this.player.loadTexture('player_right', 0);
            this.player.x += 5;
        }   
        if(this.cursor.right.isDown == false && this.cursor.left.isDown == false){
            this.player.loadTexture('player');
        }
    },
    playershoot: function(){
        if(this.spaceKey.isDown && this.player.alive == true){
            if(game.time.now > nextFire && bullets.countDead() > 0){
                shoot.play();
                shoot.volume = 0.5 * game.global.vol;
                nextFire = game.time.now + 100;
                if(nowtype == 1){
                    var bullet1 = bullets.getFirstDead();
                    bullet1.reset(this.player.x - 4, this.player.y - 40);
                }
                else if(nowtype == 2){
                    var bullet1 = bullets.getFirstDead();
                    bullet1.reset(this.player.x - 24, this.player.y - 40);
                    var bullet2 = bullets.getFirstDead();
                    bullet2.reset(this.player.x + 16, this.player.y - 40);
                }
                
                if(nowtype == 1){
                    bullet1.body.velocity.y = -300;
                }
                else if(nowtype == 2){
                    bullet1.body.velocity.y = -400;
                    bullet2.body.velocity.y = -400;
                }
            }
        }
    },
    bulletHit: function(jet, bullet){
        bullet.kill();
        var destroyed = false;
        if(this.gamestart == true) destroyed = enemies1[jet.name].damage();
        if(destroyed){
            if(this.boss == true && jet.name == 20){
                if(game.global.stage == 1) game.global.score += 1000;
                else if(game.global.stage == 2) game.global.score += 1500;
                else game.global.score += 2000;
                enemy1bullets.callAll('kill');
                gamemsg.setText('YOU WIN');
                game.time.events.add(1500, function(){
                    game.state.start('over');
                }, this);
            }
            explosion.play();
            explosion.volume = 1 * game.global.vol;
            if(game.global.stage == 1) game.global.score += 100;
            else if(game.global.stage == 2) game.global.score += 250;
            else  game.global.score += 500;
            var explosionAnimation = explosions.getFirstExists(false);
            explosionAnimation.reset(jet.x,  jet.y);
            explosionAnimation.play('kaboom', 30, false, true);
        }
        else{
            game.add.tween(enemies1[jet.name]).to({y: enemies1[jet.name].y-10}, 80).yoyo(true).start();
        }
    },
    callEnemy: function(){
        if(game.time.now > nextenemy && this.boss == false){
            if(enemiesindex >= 0 && enemiesindex < 5){
                //game.global.stage = 1;
                nextenemy = game.time.now + 1500;
                enemies1.push(new Enemy1(enemiesindex, game, this.player, enemy1bullets));
                enemiesindex ++;
            } 
            else if(enemiesindex >= 5 && enemiesindex < 10){
                //game.global.stage = 2;
                nextenemy = game.time.now + 1200;
                enemies1.push(new Enemy1(enemiesindex, game, this.player, enemy1bullets));
                enemiesindex ++;
            }
            else if(enemiesindex >= 10 && enemiesindex < 20){
                //game.global.stage = 3;
                nextenemy = game.time.now + 1000;
                enemies1.push(new Enemy1(enemiesindex, game, this.player, enemy1bullets));
                enemiesindex ++;
            }
            else if(enemiesindex == 20){
                nextenemy = game.time.now + 999999999;
                this.boss = true;
                enemies1.push(new Enemy1(enemiesindex, game, this.player, enemy1bullets));
                enemiesindex ++;
            }
        }
        /*
        if(enemiesindex >= 0 && enemiesindex < 5) game.global.stage = 1;
        else if(enemiesindex >= 5 && enemiesindex < 10) game.global.stage = 2;
        else game.global.stage = 3;
        */
    },
    movecamera: function(){
        if(game.camera.y > 0){
            this.player.y -= 1;
            game.camera.y -= 1;
        } 
    },
    gamepause: function(){
        if(game.paused==false){
            game.paused = true;
            pauseLabel.setText('Press P to continue');
        }
        else if(game.paused==true){
            game.paused = false;
            pauseLabel.setText('');
        }
    },
    volumeup: function(){
        if(game.global.vol <= 0.8) game.global.vol += 0.2;
        if(game.global.vol < 0.2) volLabel.setText('volume: 0');
        else if(game.global.vol < 0.4) volLabel.setText('volume: 1');
        else if(game.global.vol < 0.6) volLabel.setText('volume: 2');
        else if(game.global.vol < 0.8) volLabel.setText('volume: 3');
        else if(game.global.vol < 1) volLabel.setText('volume: 4');
        else volLabel.setText('volume: 5');
    },
    volumedown: function(){
        if(game.global.vol >= 0.2) game.global.vol -= 0.2;
        if(game.global.vol < 0.2) volLabel.setText('volume: 0');
        else if(game.global.vol < 0.4) volLabel.setText('volume: 1');
        else if(game.global.vol < 0.6) volLabel.setText('volume: 2');
        else if(game.global.vol < 0.8) volLabel.setText('volume: 3');
        else if(game.global.vol < 1) volLabel.setText('volume: 4');
        else volLabel.setText('volume: 5');
    },
    updatestage: function(){
        stageLabel.setText('stage: ' + game.global.stage);
    },
    playerdie: function(player, bullet){
        bullet.kill();
        live = playerlives.getFirstAlive();
        if (live){
            live.kill();
        }
        game.add.tween(player).to({x:player.x, y: player.y+10}, 80).yoyo(true).start();
        // When the player dies
        if (playerlives.countLiving() < 1){
            player.kill();
            //  And create an explosion :)
            var explosion = explosions.getFirstExists(false);
            explosion.reset(this.player.body.x, this.player.body.y);
            explosion.play('kaboom', 30, false, true);
            enemy1bullets.callAll('kill');
            this.gamestart = false;
            gamemsg.setText('YOU LOSE');
            game.time.events.add(2000, function(){
                game_bg.stop();
                game.state.start('over');
            }, this);
        }
    }
};


