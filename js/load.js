var loadState = {
    //add loading label 
    preload: function(){
        game.global.highestscore = 0;
        var loadingLabel = game.add.text(game.width/2, 150, 'loading...', {font: '30px Papyrus', fill: '#ffffff'});
        loadingLabel.anchor.setTo(0.5, 0.5);
        //display progressBar
        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        
        //game assets
        game.load.image('tileset','tileset2.png');
        game.load.tilemap('map', 'raiden_map.json', null,Phaser.Tilemap.TILED_JSON);
        
        game.load.image('player','assets/raiden_sheet0.png');
        game.load.image('player_left','assets/raiden_sheet_left.png');
        game.load.image('player_right','assets/raiden_sheet_right.png');
        game.load.image('bullet','assets/playerbullet.png');
        game.load.image('ebullet', 'assets/playerbb.png');
        game.load.image('enemy1','assets/enemy1.png');
        game.load.image('enemy2','assets/enemy2.png');
        game.load.image('enemy3','assets/enemy3.png');
        game.load.image('boss','assets/jet2.png');
        game.load.image('UFO1','assets/UFO1.png');
        game.load.image('UFO2', 'assets/UFO2.png');
        game.load.spritesheet('kaboom', 'assets/explosion.png', 64, 64, 23);
        //game.load.spritesheet('kaboom', 'assets/games/invaders/explode.png', 128, 128);
        game.load.image('pixel','assets/pixel.png');

        game.load.image('hp', 'assets/love.jpg');

        //game audio
        game.load.audio('explosion', 'audio/explosion.mp3');
        game.load.audio('playershoot', 'audio/blaster.mp3');
        game.load.audio('game_bg', 'audio/game_background.mp3');
        //load a menu background
        game.load.image('menubg','assets/menubg2.jpg');
    },
    create: function(){
        game.state.start('menu');
    }
    

}