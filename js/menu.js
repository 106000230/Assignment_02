var menuState = {
    create: function(){
        game.add.image(0, 0, 'menubg');
        //display nameof game
        var nameLabel = game.add.text(game.width/2, 70, 'Raiden', {font:'50px Papyrus', fill:'#ffffff'});
        nameLabel.anchor.setTo(0.5, 0.5);
        //show the score 
        var scoreLabel = game.add.text(game.width/2, 140, 'score: ' + game.global.score, {font:'25px Papyrus', fill:'#ffffff'});
        scoreLabel.anchor.setTo(0.5, 0.5);
        //press enter start game
        var startLabel = game.add.text(game.width/2, 210, 'press ENTER to start', {font:'25px Papyrus', fill:'#ffffff'});
        startLabel.anchor.setTo(0.5, 0.5);
        stageLabel = game.add.text(game.width/2, 280, 'Stage: ' + game.global.stage + ' (press 1 2 3 to select)', {font:'25px Papyrus', fill:'#ffffff'});
        stageLabel.anchor.setTo(0.5, 0.5);
        //key detect
        keys = game.input.keyboard.addKeys({ stage1: Phaser.Keyboard.ONE, stage2: Phaser.Keyboard.TWO, stage3: Phaser.Keyboard.THREE });
        keys.stage1.onDown.add(this.playFx, this);
        keys.stage2.onDown.add(this.playFx, this);
        keys.stage3.onDown.add(this.playFx, this);
        var keyEnter = this.input.keyboard.addKey(Phaser.KeyCode.ENTER);
        keyEnter.onDown.add(this.start, this);
    },
    playFx: function(key){
        switch (key.keyCode){
            case Phaser.Keyboard.ONE:
                game.global.stage = 1;
                break;
            case Phaser.Keyboard.TWO:
                game.global.stage = 2;
                break;
            case Phaser.Keyboard.THREE:
                game.global.stage = 3;
                break;
        }
        stageLabel.setText('Stage: ' + game.global.stage+ ' (press 1 2 3 to select)');
    },
    start: function(){
        game.global.score = 0;
        game.state.start('play');
    }
}